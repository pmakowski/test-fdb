*** Settings ***
Resource          resources/globale.txt

*** Test Cases ***
Test my values
    [Template]    Value in country table
    USA    Dollar
    England    Pound

Test not existing values
    [Template]    Value not in country table
    England    Euro

*** Keywords ***
Value in country table
    [Arguments]    ${country}    ${currency}
    Check If Exists In Database    SELECT 1 FROM COUNTRY WHERE COUNTRY='${country}' and CURRENCY='${currency}'

Value not in country table
    [Arguments]    ${country}    ${currency}
    Comment    Check If Not Exists In Database    SELECT 1 FROM COUNTRY WHERE COUNTRY='${country}' and CURRENCY='${currency}'
    Run Keyword And Expect Error    Expected to have have at least one row*    Check If Exists In Database    SELECT 1 FROM COUNTRY WHERE COUNTRY='${country}' and CURRENCY='${currency}'
